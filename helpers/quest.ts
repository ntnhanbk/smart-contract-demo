import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { BigNumber } from "ethers";

export type QuestParams = {
  questId: number;
  rewardCoin: string;
  rewardAmount: BigNumber;
  user: string;
};

export function createQuestSignature(
  wallet: SignerWithAddress,
  contractAddress: string,
  params: QuestParams,
  chainId: number,
): Promise<string> {
  const domain = {
    name: "Quest",
    version: "1.0.0",
    chainId,
    verifyingContract: contractAddress,
  };
  const types = {
    Claim: [
      { name: "questId", type: "uint256" },
      { name: "rewardCoin", type: "address" },
      { name: "rewardAmount", type: "uint256" },
      { name: "user", type: "address" },
    ],
  };

  return wallet._signTypedData(domain, types, params);
}
