import { ethers, upgrades } from "hardhat";

async function main() {
  const questAddress = process.env.QUEST_ADDRESS;
  if (!questAddress) {
    throw new Error("QUEST_ADDRESS is not set");
  }
  const Quest = await ethers.getContractFactory("Quest");
  // upgrade proxy
  const quest = await upgrades.upgradeProxy(questAddress, Quest);
  await quest.deployed();

  console.log('-----------------------------------');
  console.log('Upgraded quest');
  console.log(Date());
  console.table({
    address: quest.address,
    implementation: await upgrades.erc1967.getImplementationAddress(quest.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
