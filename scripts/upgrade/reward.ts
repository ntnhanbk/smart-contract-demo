import { ethers, upgrades } from "hardhat";

async function main() {
  const rewardAddress = process.env.REWARD_ADDRESS;
  if (!rewardAddress) {
    throw new Error("REWARD_ADDRESS is not set");
  }
  const Reward = await ethers.getContractFactory("Reward");
  // upgrade proxy
  const reward = await upgrades.upgradeProxy(rewardAddress, Reward);
  await reward.deployed();

  console.log('-----------------------------------');
  console.log('Upgraded reward');
  console.log(Date());
  console.table({
    address: reward.address,
    implementation: await upgrades.erc1967.getImplementationAddress(reward.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
