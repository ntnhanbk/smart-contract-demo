import { ethers, upgrades } from "hardhat";

async function main() {
  const marketPayee = process.env.MARKET_PAYEE;
  if (!marketPayee) {
    throw new Error("MARKET_PAYEE is not defined");
  }

  const Marketplace = await ethers.getContractFactory("Marketplace");
  const marketplace = await upgrades.deployProxy(Marketplace, [marketPayee]);
  await marketplace.deployed();

  console.log('-----------------------------------');
  console.log('Deployed marketplace');
  console.log(Date());
  console.table({
    address: marketplace.address,
    implementation: await upgrades.erc1967.getImplementationAddress(marketplace.address),
  });
  console.log(`Params: ${marketPayee}`);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
