import { ethers, upgrades } from "hardhat";

async function main() {
  const Reward = await ethers.getContractFactory("Reward");
  const reward = await upgrades.deployProxy(Reward, []);
  await reward.deployed();

  console.log('-----------------------------------');
  console.log('Deployed reward');
  console.log(Date());
  console.table({
    address: reward.address,
    implementation: await upgrades.erc1967.getImplementationAddress(reward.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
