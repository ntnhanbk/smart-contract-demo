import { ethers, upgrades } from "hardhat";

async function main() {
  const MarketFee = await ethers.getContractFactory("MarketFee");
  const marketFee = await upgrades.deployProxy(MarketFee);
  await marketFee.deployed();

  console.log('-----------------------------------');
  console.log('Deployed royalty');
  console.log(Date());
  console.table({
    address: marketFee.address,
    implementation: await upgrades.erc1967.getImplementationAddress(marketFee.address),
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
