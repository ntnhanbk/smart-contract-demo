import { ethers } from "hardhat";

async function main() {
  const offerAddress = process.env.OFFER_ADDRESS;
  const rewardAddress = process.env.REWARD_ADDRESS;
  const marketAddress = process.env.MARKET_ADDRESS;

  if (!offerAddress || !rewardAddress || !marketAddress) {
    throw new Error("OFFER_ADDRESS and REWARD_ADDRESS and MARKET_ADDRESS must be set");
  }

  const market = await ethers.getContractAt("Marketplace", marketAddress);
  await market.setReward(rewardAddress);

  const offer = await ethers.getContractAt("Offer", offerAddress);
  await offer.setReward(rewardAddress);

  const reward = await ethers.getContractAt("Reward", rewardAddress);
  await reward.setTrusted(marketAddress, true);
  await reward.setTrusted(offerAddress, true);

  console.log('-----------------------------------');
  console.log('Config reward');
  console.log(Date());
  console.table({
    offer: offer.address,
    market: market.address,
    reward: rewardAddress,
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
