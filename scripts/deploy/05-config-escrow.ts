import { ethers } from "hardhat";

async function main() {
  const offerAddress = process.env.OFFER_ADDRESS;
  const escrowAddress = process.env.ESCROW_ADDRESS;

  if (!offerAddress || !escrowAddress) {
    throw new Error("OFFER_ADDRESS and ESCROW_ADDRESS is not defined");
  }

  const offer = await ethers.getContractAt("Offer", offerAddress);
  await offer.setEscrow(escrowAddress);

  const escrow = await ethers.getContractAt("Escrow", escrowAddress);
  await escrow.setTrusted(offerAddress, true);

  console.log('-----------------------------------');
  console.log('Config escrow');
  console.log(Date());
  console.table({
    offer: offer.address,
    escrow: escrowAddress,
  });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
