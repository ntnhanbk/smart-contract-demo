import { expect } from "chai";
import { BigNumber, constants } from "ethers";
import { ethers } from "hardhat";
import { createMintSignature, MintParams, PaymentType } from "../helpers/launchpad";

describe("Launchpad", function() {
  async function deploy() {
    const [owner, user1, user2, user3, signer, payee1, payee2, corgiPayee] = await ethers.getSigners();
    const Launchpad = await ethers.getContractFactory("Launchpad");
    const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    const ERC721Mock = await ethers.getContractFactory("NFTMock");

    const nft1 = await ERC721Mock.deploy("nft1", "nft1");
    const nft2 = await ERC721Mock.deploy("nft2", "nft2");

    const coin1 = await ERC20Mock.deploy("coin1", "coin1");
    const coin2 = await ERC20Mock.deploy("coin2", "coin2");

    const launchpad = await Launchpad.deploy(
      "name", "symbol", "https://uri.com/metadata/", 30, 10, [payee1.address, payee2.address], [2500, 7500], signer.address, corgiPayee.address
    );

    // transfer coin to user
    await coin1.transfer(user1.address, ethers.utils.parseEther("100"));
    await coin2.transfer(user1.address, ethers.utils.parseEther("100"));

    // mint nft for user
    await nft1.mint(user1.address, 1);
    await nft1.mint(user1.address, 2);
    await nft1.mint(user1.address, 3);
    await nft1.mint(user1.address, 4);

    await nft2.mint(user1.address, 1);
    await nft2.mint(user1.address, 2);

    const chainId = 31337;
    return { launchpad, owner, user1, user2, user3, signer, chainId, payee1, payee2, corgiPayee, nft1, nft2, coin1, coin2 };
  }

  describe("Deployment", function() {
    it("Should set the right owner", async function() {
      const { launchpad, owner } = await deploy();
      expect(await launchpad.owner()).to.equal(owner.address);
    });
  });

  describe("Mint", function() {

    describe("Validate", function() {
      it("Revert r2 if exeed userLimit and not owner mint", async function() {
        const { launchpad, user1, chainId, signer } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(11),
          tokens: [constants.AddressZero],
          tokenAmounts: [ethers.utils.parseEther("20")],
          value: ethers.utils.parseEther("20")
        }

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        )

        await expect(launchpad.connect(user1).mint(
          user1.address,
          mintParams.quantity,
          [PaymentType.NATIVE],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
          {
            value: mintParams.value,
          }
        )).to.be.revertedWith("r2");
      })

      it("It should avoid userLimit if owner mint", async function() {
        const { launchpad, user1, chainId, signer, owner } = await deploy();

        const mintParams: MintParams = {
          from: owner.address,
          quantity: BigNumber.from(11),
          tokens: [constants.AddressZero],
          tokenAmounts: [ethers.utils.parseEther("0")],
          value: ethers.utils.parseEther("0")
        }

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        )

        await launchpad.connect(owner).mint(
          user1.address,
          mintParams.quantity,
          [PaymentType.NATIVE],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        expect(await launchpad.balanceOf(user1.address)).to.equal(11);
      })
    });

    describe("Single payment", function() {
      it("mint by native", async function() {
        const { launchpad, signer, user1, chainId, payee1, payee2, corgiPayee } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(5),
          tokens: [constants.AddressZero],
          tokenAmounts: [ethers.utils.parseEther("20")],
          value: ethers.utils.parseEther("20")
        }

        // // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        )

        const mintTx = launchpad.connect(user1).mint(
          user1.address,
          mintParams.quantity,
          [PaymentType.NATIVE],
          mintParams.tokens,
          [ethers.utils.parseEther("20")],
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        // check balance of payees use expect change eth
        const corgiBalance = ethers.utils.parseEther("20").mul(5).div(100);
        const payeesBalance = ethers.utils.parseEther("20").sub(corgiBalance);

        await expect(mintTx).to.changeEtherBalance(corgiPayee, corgiBalance);
        await expect(mintTx).to.changeEtherBalance(payee1, payeesBalance.mul(25).div(100));
        await expect(mintTx).to.changeEtherBalance(payee2, payeesBalance.mul(75).div(100));

        // check nft balance
        expect(await launchpad.balanceOf(user1.address)).to.equal(5);

        // check toltal supply
        expect(await launchpad.totalSupply()).to.equal(5);

        // check token uri
        expect(await launchpad.tokenURI(1)).to.equal("https://uri.com/metadata/1.json");
        expect(await launchpad.tokenURI(5)).to.equal("https://uri.com/metadata/5.json");

        // check mint limit
        expect(await launchpad.minted(user1.address)).to.equal(5);
      });

      it("mint by erc20", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, coin1 } = await deploy();

        // get balance before mint
        const user1BalanceBefore = await coin1.balanceOf(user1.address);
        const payee1BalanceBefore = await coin1.balanceOf(payee1.address);
        const payee2BalanceBefore = await coin1.balanceOf(payee2.address);
        const corgiPayeeBalanceBefore = await coin1.balanceOf(corgiPayee.address);

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(6),
          tokens: [coin1.address],
          tokenAmounts: [ethers.utils.parseEther("23")],
          value: ethers.utils.parseEther("0")
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await coin1.connect(user1).approve(launchpad.address, ethers.utils.parseEther("23"));

        await launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC20],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
        );

        // check balance of payees use expect change coin balance
        const corgiBalance = ethers.utils.parseEther("23").mul(10).div(100);
        const payeesBalance = ethers.utils.parseEther("23").sub(corgiBalance);

        expect(await coin1.balanceOf(user1.address)).to.equal(user1BalanceBefore.sub(ethers.utils.parseEther("23")));
        expect(await coin1.balanceOf(corgiPayee.address)).to.equal(corgiPayeeBalanceBefore.add(corgiBalance));
        expect(await coin1.balanceOf(payee1.address)).to.equal(payee1BalanceBefore.add(payeesBalance.mul(25).div(100)));
        expect(await coin1.balanceOf(payee2.address)).to.equal(payee2BalanceBefore.add(payeesBalance.mul(75).div(100)));
      });

      it("mint by erc721", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, corgiPayee, nft1 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [nft1.address],
          tokenAmounts: [BigNumber.from(2)],
          value: ethers.utils.parseEther("9") // 3 CRO per nft
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 nft
        await nft1.connect(user1).approve(launchpad.address, 1);
        await nft1.connect(user1).approve(launchpad.address, 2);

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC721],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [1, 2],
          signature,
          {
            value: mintParams.value
          }
        );

        await mintTx;

        // check balance of payees
        const corgiBalance = ethers.utils.parseEther("9");
        await expect(mintTx).to.changeEtherBalance(corgiPayee, corgiBalance);

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
        expect(await nft1.balanceOf(payee1.address)).to.equal(2);

        expect(await nft1.ownerOf(1)).to.equal(payee1.address);
        expect(await nft1.ownerOf(2)).to.equal(payee1.address);

        // uri, total supply
        expect(await launchpad.tokenURI(1)).to.equal("https://uri.com/metadata/1.json");
        expect(await launchpad.tokenURI(2)).to.equal("https://uri.com/metadata/2.json");
        expect(await launchpad.tokenURI(3)).to.equal("https://uri.com/metadata/3.json");

        expect(await launchpad.totalSupply()).to.equal(3);
      });
    });

    describe("mint double", function() {
      it("mint by native + erc20", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, coin1 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(5),
          tokens: [ethers.constants.AddressZero, coin1.address],
          tokenAmounts: [ethers.utils.parseEther("20"), ethers.utils.parseEther("23")],
          value: ethers.utils.parseEther("20")
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await coin1.connect(user1).approve(launchpad.address, ethers.utils.parseEther("23"));

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.NATIVE, PaymentType.ERC20],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
          {
            value: mintParams.value
          },
        );

        // check native balance of payees
        const corgiBalance = ethers.utils.parseEther("20").mul(5).div(100);
        const payeesBalance = ethers.utils.parseEther("20").sub(corgiBalance);

        await expect(mintTx).to.changeEtherBalance(corgiPayee, corgiBalance);
        await expect(mintTx).to.changeEtherBalance(payee1, payeesBalance.mul(25).div(100));
        await expect(mintTx).to.changeEtherBalance(payee2, payeesBalance.mul(75).div(100));

        await expect(mintTx).to.changeEtherBalance(payee1, payeesBalance.mul(25).div(100));
        await expect(mintTx).to.changeEtherBalance(payee2, payeesBalance.mul(75).div(100));

        // check erc20 balance of payees
        const corgiCoinBalance = ethers.utils.parseEther("23").mul(10).div(100);
        const payeesCoinBalance = ethers.utils.parseEther("23").sub(corgiCoinBalance);

        await expect(mintTx).to.changeTokenBalance(coin1, corgiPayee, corgiCoinBalance);
        await expect(mintTx).to.changeTokenBalance(coin1, payee1, payeesCoinBalance.mul(25).div(100));
        await expect(mintTx).to.changeTokenBalance(coin1, payee2, payeesCoinBalance.mul(75).div(100));

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(5);
      });

      it("mint by native + erc721", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, nft1 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [nft1.address, ethers.constants.AddressZero],
          tokenAmounts: [BigNumber.from(2), ethers.utils.parseEther("6")],
          value: ethers.utils.parseEther("9").add(ethers.utils.parseEther("6")) // 3 CRO per NFT + 6 CRO for native
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 nft
        await nft1.connect(user1).approve(launchpad.address, 1);
        await nft1.connect(user1).approve(launchpad.address, 2);

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC721, PaymentType.NATIVE],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [1, 2],
          signature,
          {
            value: mintParams.value,
          }
        );

        await mintTx;

        // check native balance of payees
        const corgiBalance = ethers.utils.parseEther("9").add(
          ethers.utils.parseEther("6").mul(5).div(100)
        ); // (3 CRO per NFT * 3) + (6 CRO for native * 5%)

        const payeesBalance = ethers.utils.parseEther("6").sub(
          ethers.utils.parseEther("6").mul(5).div(100)
        );

        await expect(mintTx).to.changeEtherBalance(corgiPayee, corgiBalance);
        await expect(mintTx).to.changeEtherBalance(payee1, payeesBalance.mul(25).div(100));
        await expect(mintTx).to.changeEtherBalance(payee2, payeesBalance.mul(75).div(100));

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
        expect(await nft1.balanceOf(payee1.address)).to.equal(2);

        expect(await nft1.ownerOf(1)).to.equal(payee1.address);
        expect(await nft1.ownerOf(2)).to.equal(payee1.address);

        // uri, total supply
        expect(await launchpad.tokenURI(1)).to.equal("https://uri.com/metadata/1.json");
        expect(await launchpad.tokenURI(2)).to.equal("https://uri.com/metadata/2.json");
        expect(await launchpad.tokenURI(3)).to.equal("https://uri.com/metadata/3.json");

        expect(await launchpad.totalSupply()).to.equal(3);
      });

      it("mint by erc20 + erc721", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, coin1, nft1 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [nft1.address, coin1.address],
          tokenAmounts: [BigNumber.from(2), ethers.utils.parseEther("23")],
          value: ethers.utils.parseEther("3").mul(3) // 3 CRO per NFT
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await coin1.connect(user1).approve(launchpad.address, ethers.utils.parseEther("23"));

        // approve launchpad to spend user1 nft
        await nft1.connect(user1).approve(launchpad.address, 1);
        await nft1.connect(user1).approve(launchpad.address, 2);

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC721, PaymentType.ERC20],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [1, 2],
          signature,
          {
            value: mintParams.value,
          }
        );

        await mintTx;

        // check erc20 balance of payees
        const corgiCoinBalance = ethers.utils.parseEther("23").mul(10).div(100);
        const payeesCoinBalance = ethers.utils.parseEther("23").sub(corgiCoinBalance);

        await expect(mintTx).to.changeTokenBalance(coin1, corgiPayee, corgiCoinBalance);
        await expect(mintTx).to.changeTokenBalance(coin1, payee1, payeesCoinBalance.mul(25).div(100));
        await expect(mintTx).to.changeTokenBalance(coin1, payee2, payeesCoinBalance.mul(75).div(100));

        // check native balance of payees
        const corgiBalance = ethers.utils.parseEther("3").mul(3); // 3 CRO per NFT * 3
        await expect(mintTx).to.changeEtherBalance(corgiPayee, corgiBalance);

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
        expect(await nft1.balanceOf(payee1.address)).to.equal(2);

        expect(await nft1.ownerOf(1)).to.equal(payee1.address);
        expect(await nft1.ownerOf(2)).to.equal(payee1.address);
      });

      it("mint by erc20 + erc20", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, coin1, coin2 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [coin1.address, coin2.address],
          tokenAmounts: [ethers.utils.parseEther("23"), ethers.utils.parseEther("50")],
          value: BigNumber.from(0)
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await coin1.connect(user1).approve(launchpad.address, ethers.utils.parseEther("23"));
        await coin2.connect(user1).approve(launchpad.address, ethers.utils.parseEther("50"));

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC20, PaymentType.ERC20],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        await mintTx;

        // check erc20 balance of payees
        const corgiCoin1Balance = ethers.utils.parseEther("23").mul(10).div(100);
        const payeesCoin1Balance = ethers.utils.parseEther("23").sub(corgiCoin1Balance);

        const corgiCoin2Balance = ethers.utils.parseEther("50").mul(10).div(100);
        const payeesCoin2Balance = ethers.utils.parseEther("50").sub(corgiCoin2Balance);

        await expect(mintTx).to.changeTokenBalance(coin1, corgiPayee, corgiCoin1Balance);
        await expect(mintTx).to.changeTokenBalance(coin1, payee1, payeesCoin1Balance.mul(25).div(100));
        await expect(mintTx).to.changeTokenBalance(coin1, payee2, payeesCoin1Balance.mul(75).div(100));

        await expect(mintTx).to.changeTokenBalance(coin2, corgiPayee, corgiCoin2Balance);
        await expect(mintTx).to.changeTokenBalance(coin2, payee1, payeesCoin2Balance.mul(25).div(100));
        await expect(mintTx).to.changeTokenBalance(coin2, payee2, payeesCoin2Balance.mul(75).div(100));

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
      });

      it("mint by erc721 + erc721", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, nft1, nft2 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [nft1.address, nft2.address],
          tokenAmounts: [BigNumber.from(3), BigNumber.from(2)],
          value: ethers.utils.parseEther("3").mul(3).mul(2)
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await nft1.connect(user1).approve(launchpad.address, 1);
        await nft1.connect(user1).approve(launchpad.address, 2);
        await nft1.connect(user1).approve(launchpad.address, 3);

        await nft2.connect(user1).approve(launchpad.address, 1);
        await nft2.connect(user1).approve(launchpad.address, 2);

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC721, PaymentType.ERC721],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [1, 2, 3, 1, 2],
          signature,
          {
            value: mintParams.value,
          }
        );

        await mintTx;

        // check nft balance
        expect(await nft1.balanceOf(payee1.address)).to.equal(3);
        expect(await nft2.balanceOf(payee1.address)).to.equal(2);

        expect(await nft1.ownerOf(1)).to.equal(payee1.address);
        expect(await nft1.ownerOf(2)).to.equal(payee1.address);
        expect(await nft1.ownerOf(3)).to.equal(payee1.address);
        expect(await nft2.ownerOf(1)).to.equal(payee1.address);
        expect(await nft2.ownerOf(2)).to.equal(payee1.address);

        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
      })

      it("mint by erc20 + erc20, same token", async function() {
        const { launchpad, signer, user1, user2, chainId, payee1, payee2, corgiPayee, coin1 } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(3),
          tokens: [coin1.address, coin1.address],
          tokenAmounts: [ethers.utils.parseEther("23"), ethers.utils.parseEther("50")],
          value: BigNumber.from(0)
        };

        // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        );

        // approve launchpad to spend user1 coin
        await coin1.connect(user1).approve(launchpad.address, ethers.utils.parseEther("73"));

        const mintTx = launchpad.connect(user1).mint(
          user2.address,
          mintParams.quantity,
          [PaymentType.ERC20, PaymentType.ERC20],
          mintParams.tokens,
          mintParams.tokenAmounts,
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        await mintTx;

        // check erc20 balance of payees
        const corgiCoin1Balance = ethers.utils.parseEther("73").mul(10).div(100);
        const payeesCoin1Balance = ethers.utils.parseEther("73").sub(corgiCoin1Balance);

        const corgiCoin2Balance = ethers.utils.parseEther("50").mul(10).div(100);
        const payeesCoin2Balance = ethers.utils.parseEther("50").sub(corgiCoin2Balance);

        await expect(mintTx).to.changeTokenBalance(coin1, corgiPayee, corgiCoin1Balance);
        await expect(mintTx).to.changeTokenBalance(coin1, payee1, payeesCoin1Balance.mul(25).div(100));
        await expect(mintTx).to.changeTokenBalance(coin1, payee2, payeesCoin1Balance.mul(75).div(100));

        // check nft balance
        expect(await launchpad.balanceOf(user2.address)).to.equal(3);
      });
    })
  });

  describe("Mask tokenURI", function() {
    it("Should don't mask tokenURI by default", async function() {
        const { launchpad, signer, user1, chainId, payee1, payee2, corgiPayee } = await deploy();

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(5),
          tokens: [constants.AddressZero],
          tokenAmounts: [ethers.utils.parseEther("20")],
          value: ethers.utils.parseEther("20")
        }

        // // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        )

        await launchpad.connect(user1).mint(
          user1.address,
          mintParams.quantity,
          [PaymentType.NATIVE],
          mintParams.tokens,
          [ethers.utils.parseEther("20")],
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        expect(await launchpad.tokenURI(1)).to.equal("https://uri.com/metadata/1.json");
        expect(await launchpad.tokenURI(5)).to.equal("https://uri.com/metadata/5.json");
    })

    it("Should mask tokenURI after set mask link and unmask", async function() {
        const { launchpad, signer, user1, chainId, owner } = await deploy();

        // set mask link
        await launchpad.connect(owner).maskUri("masked");

        const mintParams: MintParams = {
          from: user1.address,
          quantity: BigNumber.from(5),
          tokens: [constants.AddressZero],
          tokenAmounts: [ethers.utils.parseEther("20")],
          value: ethers.utils.parseEther("20")
        }

        // // create signature
        const signature = await createMintSignature(
          signer,
          launchpad.address,
          mintParams,
          chainId,
        )

        await launchpad.connect(user1).mint(
          user1.address,
          mintParams.quantity,
          [PaymentType.NATIVE],
          mintParams.tokens,
          [ethers.utils.parseEther("20")],
          [],
          signature,
          {
            value: mintParams.value,
          }
        );

        expect(await launchpad.tokenURI(1)).to.equal("masked");
        expect(await launchpad.tokenURI(5)).to.equal("masked");

        // unmask
        await launchpad.connect(owner).maskUri("");

        expect(await launchpad.tokenURI(1)).to.equal("https://uri.com/metadata/1.json");
        expect(await launchpad.tokenURI(5)).to.equal("https://uri.com/metadata/5.json");
    })
  });
});
