import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { formatEther } from "ethers/lib/utils";
import { ethers } from "hardhat";

describe("RewardDistribute", function() {
  async function deploy() {
    const [owner, user1, user2, user3, corgiPayee] =
      await ethers.getSigners();

    const ERC721 = await ethers.getContractFactory("NftEnumerate");
    const ERC20 = await ethers.getContractFactory("ERC20Mock");
    const RewardDistribute = await ethers.getContractFactory("RewardDistribute");

    const collection = await ERC721.deploy();
    await collection.deployed();

    const corgi = await ERC721.deploy();
    await corgi.deployed();

    const rewardToken = await ERC20.deploy("Reward Token", "RT");
    await rewardToken.deployed();

    // mint some nft
    await collection.safeMint(user1.address, 1);
    await collection.safeMint(user1.address, 2);
    await collection.safeMint(user1.address, 3);

    await corgi.safeMint(user3.address, 1);

    const reward = await RewardDistribute.deploy(
      collection.address,
      rewardToken.address,
      owner.address,
      corgi.address,
      corgiPayee.address,
      ethers.utils.parseEther("0.01"),
      {
        value: ethers.utils.parseEther("0.01"),
      }
    );

    return { owner, user1, user2, user3, reward, collection, rewardToken, corgiPayee };
  }

  describe("Deployment", function() {
    it("Should set the right config", async function() {
      const { owner, reward, collection, user1, user3 } = await loadFixture(deploy);
      expect(await reward.owner()).to.equal(owner.address);
      expect(await reward.collection()).to.equal(collection.address);

      expect(await reward.getClaimFee(user1.address)).to.equal(await reward.fee());
      expect(await reward.getClaimFee(user3.address)).to.equal(await reward.corgiFee());
    });
  });

  describe("Setting", function() {
    it("Should change owner", async function() {
      const { reward, user1 } = await loadFixture(deploy);
      await reward.transferOwnership(user1.address);
      expect(await reward.owner()).to.equal(user1.address);
    });
  });

  describe("createDistribute", function() {
    it("Should create distribute", async function() {
      const { reward, user1, user2, user3, collection, rewardToken } = await loadFixture(deploy);

      const amount1 = ethers.utils.parseEther("100");
      const distribute1 = amount1.mul(97).div(100);
      await rewardToken.approve(reward.address, amount1);

      await reward.createDistribute(amount1);

      // check
      expect(await reward.totalRoyalties()).to.equal(distribute1);
      const royalty1 = await reward.currentRoyalties();
      expect(royalty1).to.equal(distribute1.div(3));

      expect(await reward.getRewardsToken(1)).to.equal(royalty1);
      expect(await reward.getRoyalties(user1.address)).to.equal(royalty1.mul(3));

      // add more royalty
      const amount2 = ethers.utils.parseEther("200");
      const distribute2 = amount2.mul(97).div(100);
      await rewardToken.approve(reward.address, amount2);
      await reward.createDistribute(amount2);

      expect(await reward.totalRoyalties()).to.equal(distribute1.add(distribute2));
      const royalty2 = await reward.currentRoyalties();
      expect(royalty2).to.equal(distribute2.div(3).add(royalty1));

      expect(await reward.getRewardsToken(1)).to.equal(royalty2);
      expect(await reward.getRoyalties(user1.address)).to.equal(royalty2.mul(3));
    });
  });

  describe("claim", function() {
    it("Should claim", async function() {
      const { reward, user1, rewardToken, corgiPayee } = await loadFixture(deploy);

      const amount1 = ethers.utils.parseEther("100");
      const distribute1 = amount1.mul(97).div(100);
      await rewardToken.approve(reward.address, amount1);
      await reward.createDistribute(amount1);

      const royalty1 = await reward.currentRoyalties();

      // claim balance of token 1
      const claimFee = await reward.getClaimFee(user1.address);
      const claimTx = reward.connect(user1).claimRoyalties([1], { value: claimFee });
      await claimTx;

      expect(await reward.getRewardsToken(1)).to.equal(0);

      // check balance
      expect(await rewardToken.balanceOf(user1.address)).to.equal(royalty1);
      expect(claimTx).to.changeEtherBalance(corgiPayee, claimFee);

      // add more royalty
      const amount2 = ethers.utils.parseEther("200");
      const distribute2 = amount2.mul(97).div(100);
      await rewardToken.approve(reward.address, amount2);
      await reward.createDistribute(amount2);

      expect(await reward.totalRoyalties()).to.equal(distribute2.add(distribute1));
      const royalty2 = await reward.currentRoyalties();
      expect(royalty2).to.equal(distribute2.div(3).add(royalty1));

      expect(await reward.getRewardsToken(1)).to.equal(royalty2.sub(royalty1));
      expect(await reward.getRoyalties(user1.address)).to.equal(royalty2.mul(3).sub(royalty1));

      const royaltyDetails = await reward.getRoyaltiesDetails(user1.address);
      expect(royaltyDetails.length).to.equal(3);

      expect(royaltyDetails[0].id).to.equal(1);
      expect(royaltyDetails[0].royalties).to.equal(royalty2.sub(royalty1));

      expect(royaltyDetails[1].id).to.equal(2);
      expect(royaltyDetails[1].royalties).to.equal(royalty2);
    });
  });
});
