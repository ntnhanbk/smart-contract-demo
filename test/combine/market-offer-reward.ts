import { expect } from "chai";
import { ethers, upgrades } from "hardhat";
import { createListSignature } from "../../helpers/market";
import { caeateOfferSignature, OfferParams } from "../../helpers/offer";

describe("buy-flow", function () {
  async function deploy() {
    const [owner, user1, user2, user3, user4] = await ethers.getSigners();
    const Marketplace = await ethers.getContractFactory("Marketplace");
    const Offer = await ethers.getContractFactory("Offer");
    const Nft = await ethers.getContractFactory("NFTMock");
    const Escrow = await ethers.getContractFactory("Escrow");
    const Royal = await ethers.getContractFactory("Royalty");
    const Reward = await ethers.getContractFactory("Reward");

    // deploy marketplace
    const marketplace = await upgrades.deployProxy(Marketplace, [
      owner.address,
    ]);
    await marketplace.deployed();

    // deploy offer
    const offer = await upgrades.deployProxy(Offer, [owner.address]);
    await offer.deployed();

    const nft = await Nft.deploy("nft", "nft");
    const nft2 = await Nft.deploy("nft2", "nf2");

    await nft.mint(user1.address, 3); // user1: 1, 2, 3
    await nft.mint(user2.address, 1); // user2: 4
    await nft.mint(user3.address, 1); // user3: 5

    await nft2.mint(user1.address, 1); // user1: 1

    // deploy royaltyRegistry
    const royaltyRegistry = await upgrades.deployProxy(Royal, []);
    await marketplace.setRoyaltyRegistry(royaltyRegistry.address);

    // register royal
    await royaltyRegistry.setRoyalty(nft.address, user3.address, 1000); // 10 %

    // deploy reward
    const reward = await upgrades.deployProxy(Reward, []);
    await reward.deployed();

    // set tiers
    await reward.setTier(1, {
      seller: ethers.utils.parseEther("0.1"), // 0.1 coin per 1 CRO
      buyer: ethers.utils.parseEther("0.2"), // 0.2 coin per 1 CRO
      collectionOwner: ethers.utils.parseEther("0.3"), // 0.3 coin per 1 CRO
    });

    await reward.setTier(2, {
      seller: ethers.utils.parseEther("0.2"),
      buyer: ethers.utils.parseEther("0.3"),
      collectionOwner: ethers.utils.parseEther("0.4"),
    });

    await reward.setCollectionToPayee(nft.address, user4.address);
    await reward.setCollectionToTier(nft.address, 2);

    // set trusted
    await reward.setTrusted(marketplace.address, true);
    await reward.setTrusted(offer.address, true);

    // deploy coin
    const Coin = await ethers.getContractFactory("ERC20Mock");
    const coin = await Coin.deploy("coin", "coin");
    await coin.deployed();

    // set rewardCoin
    await reward.setRewardToken(coin.address);
    await coin.transfer(reward.address, ethers.utils.parseEther("100"));

    // set reward
    await marketplace.setReward(reward.address);
    await offer.setReward(reward.address);

    // deploy escrow
    const escrow = await upgrades.deployProxy(Escrow, []);
    await escrow.deployed();

    // set escrow
    await offer.setEscrow(escrow.address);
    await escrow.setTrusted(offer.address, true);

    const chainId = 31337;

    return {
      marketplace,
      offer,
      nft,
      owner,
      user1,
      user2,
      user3,
      nft2,
      user4,
      chainId,
      reward,
      coin,
      escrow,
    };
  }

  describe("buyNFT", function () {
    it("Should buy NFT", async function () {
      const {
        marketplace,
        nft,
        user1,
        user2,
        user3,
        owner,
        chainId,
        reward,
        user4,
      } = await deploy();
      await nft.connect(user1).setApprovalForAll(marketplace.address, true);
      const signature = await createListSignature(
        user1,
        marketplace.address,
        {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        },
        chainId
      );

      const value = ethers.utils.parseEther("1");
      const buy = marketplace.connect(user2).buyNFT(
        {
          tokenAddress: nft.address,
          tokenId: 1,
          price: value,
          seller: user1.address,
        },
        signature,
        {
          value,
        }
      );

      // user2: buyer | user1: seller | owner: marketPayee | user3: collection payee
      await expect(buy).to.changeEtherBalances(
        [user2, user1, user3, owner],
        [
          ethers.utils.parseEther("-1"), // buyer pay 1 eth
          value.mul(85).div(100), // seller get 85% of 1 eth
          value.mul(10).div(100), // collection get 10% of 1 eth
          value.mul(5).div(100), // market get 5% of 1 eth
        ]
      );

      // check balance of reward
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.2")
      );

      expect(await reward.balanceOf(user4.address)).to.equal(
        ethers.utils.parseEther("0.4")
      );
    });

    it("Should buy NFT bundle", async function () {
      const {
        marketplace,
        nft,
        user1,
        user2,
        user3,
        owner,
        chainId,
        reward,
        user4,
      } = await deploy();
      await nft.connect(user1).setApprovalForAll(marketplace.address, true);
      const signature = await createListSignature(
        user1,
        marketplace.address,
        {
          tokenAddress: nft.address,
          tokenId: 1,
          price: ethers.utils.parseEther("1"),
          seller: user1.address,
        },
        chainId
      );

      const value = ethers.utils.parseEther("1");
      const buy = marketplace.connect(user2).buyBundle(
        [
          {
            tokenAddress: nft.address,
            tokenId: 1,
            price: value,
            seller: user1.address,
          },
        ],
        [signature],
        {
          value,
        }
      );

      // user2: buyer | user1: seller | owner: marketPayee | user3: collection payee
      await expect(buy).to.changeEtherBalances(
        [user2, user1, user3, owner],
        [
          ethers.utils.parseEther("-1"), // buyer pay 1 eth
          value.mul(85).div(100), // seller get 85% of 1 eth
          value.mul(10).div(100), // collection get 10% of 1 eth
          value.mul(5).div(100), // market get 5% of 1 eth
        ]
      );

      // check balance of reward
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.2")
      );

      expect(await reward.balanceOf(user4.address)).to.equal(
        ethers.utils.parseEther("0.4")
      );
    });

    it("Should accept offer", async function () {
      const { offer, user1, user2, nft, chainId, escrow, reward, user4 } = await deploy();
      await nft.mint(user1.address, 1);
      await nft.connect(user1).approve(offer.address, 1);

      // create offer
      await escrow.connect(user2).depositAndApprove(offer.address, {
        value: ethers.utils.parseEther("1"),
      });

      const offerParams: OfferParams = {
        tokenAddress: nft.address,
        tokenId: 1,
        price: ethers.utils.parseEther("1"),
        bidder: user2.address,
      };

      // accept offer
      const signature = await caeateOfferSignature(
        user2,
        offer.address,
        offerParams,
        chainId
      );
      await offer.connect(user1).acceptOffer(offerParams, signature);

      // check balance of reward
      expect(await reward.balanceOf(user2.address)).to.equal(
        ethers.utils.parseEther("0.3")
      );

      expect(await reward.balanceOf(user1.address)).to.equal(
        ethers.utils.parseEther("0.2")
      );

      expect(await reward.balanceOf(user4.address)).to.equal(
        ethers.utils.parseEther("0.4")
      );
    });
  });
});
