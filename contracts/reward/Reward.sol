// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import './IReward.sol';

contract Reward is IReward, Initializable, OwnableUpgradeable {
  using SafeMathUpgradeable for uint256;

  IERC20 public override rewardToken;
  mapping(uint256 => Tier) public override tiers;
  mapping(address => address) public override collectionToPayee;
  mapping(address => uint256) public override collectionToTier;
  mapping(address => uint256) public override balanceOf;
  mapping(address => bool) public override trusted;
  uint256 public override totalBalance;

  function initialize() public initializer {
    __Ownable_init();
  }

  function setRewardToken(address _rewardToken) external override onlyOwner {
    rewardToken = IERC20(_rewardToken);
  }

  function setTier(uint256 _tier, Tier calldata _tierData) external override onlyOwner {
    tiers[_tier] = _tierData;
  }

  function setCollectionToPayee(address _collectionAddress, address _payee) external override onlyOwner {
    collectionToPayee[_collectionAddress] = _payee;
  }

  function setCollectionToTier(address _collectionAddress, uint256 _tier) external override onlyOwner {
    collectionToTier[_collectionAddress] = _tier;
  }

  function setTrusted(address _address, bool _isTrusted) external override onlyOwner {
    trusted[_address] = _isTrusted;
  }

  function reward(address _seller, address _buyer, address _collectionAddress, uint256 _price) external override {
    require(trusted[_msgSender()], "Reward: not trusted");
    Tier memory tier = tiers[collectionToTier[_collectionAddress]];
    uint256 sellerReward = _price.mul(tier.seller).div(1e18);
    uint256 buyerReward = _price.mul(tier.buyer).div(1e18);
    uint256 collectionOwnerReward = _price.mul(tier.collectionOwner).div(1e18);

    balanceOf[_seller] = balanceOf[_seller].add(sellerReward);
    balanceOf[_buyer] = balanceOf[_buyer].add(buyerReward);
    totalBalance = totalBalance.add(sellerReward).add(buyerReward).add(collectionOwnerReward);
    if (collectionToPayee[_collectionAddress] != address(0)) {
      balanceOf[collectionToPayee[_collectionAddress]] = balanceOf[collectionToPayee[_collectionAddress]].add(collectionOwnerReward);
      totalBalance = totalBalance.add(collectionOwnerReward);
    }

    emit IReward.Rewarded(sellerReward, buyerReward, collectionOwnerReward, _price, collectionToTier[_collectionAddress]);
  }

  function claim() external override {
    require(balanceOf[_msgSender()] > 0, "Reward: no reward to claim");
    require(address(rewardToken) != address(0), "Reward: reward token not set");
    require(rewardToken.balanceOf(address(this)) >= balanceOf[_msgSender()], "Reward: reward token not enough");
    uint256 amount = balanceOf[msg.sender];
    balanceOf[msg.sender] = 0;
    rewardToken.transfer(msg.sender, amount);
    totalBalance = totalBalance.sub(amount);
  }
}
