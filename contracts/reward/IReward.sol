// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

// reward coin for seller, buyer, collection owner when nft bought
interface IReward {
  struct Tier {
    uint256 seller; // reward for seller per 1CRO
    uint256 buyer; // reward for buyer per 1CRO
    uint256 collectionOwner; // reward for collection owner per 1CRO
  }

  // getter
  function rewardToken() external view returns (IERC20);

  function tiers(uint256 _tier) external view returns (uint256 seller, uint256 buyer, uint256 collectionOwner);

  function collectionToPayee(address _collectionAddress) external view returns (address payee);

  function collectionToTier(address _collectionAddress) external view returns (uint256 tier);

  function balanceOf(address _account) external view returns (uint256);

  function totalBalance() external view returns (uint256);

  function trusted(address _address) external view returns (bool);

  // setter
  function setTier(uint256 _tier, Tier calldata _tierData) external;

  function setRewardToken(address _rewardCoin) external;

  function setCollectionToPayee(address _collectionAddress, address _payee) external;

  function setCollectionToTier(address _collectionAddress, uint256 _tier) external;

  function setTrusted(address _address, bool _isTrusted) external;

  // function
  function reward(address _seller, address _buyer, address _collectionAddress, uint256 _price) external;

  function claim() external;

  // event
  event Rewarded(uint256 _sellerReward, uint256 _buyerReward, uint256 _collectionOwnerReward, uint256 _price, uint256 _tier);
}
