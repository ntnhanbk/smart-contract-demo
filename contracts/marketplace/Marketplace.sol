// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/EIP712Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

import "../royalty/IRoyalty.sol";
import "../reward/IReward.sol";
import "./IMarketplace.sol";
import "../market-fee/IMarketFee.sol";

contract Marketplace is
    Initializable,
    EIP712Upgradeable,
    IMarketplace,
    ReentrancyGuardUpgradeable,
    OwnableUpgradeable
{
    using SafeMathUpgradeable for uint256;
    using SafeMathUpgradeable for uint16;

    IRoyalty public royaltyRegistry;
    address public marketPayee;
    uint16 public marketPercent;
    IReward public reward;
    IMarketFee public feeRegistry;

    function initialize(address _payee) public initializer {
        __EIP712_init("Marketplace", "1.0.0");
        __Ownable_init();
        __ReentrancyGuard_init();
        marketPayee = _payee;
        marketPercent = 500;
    }

    // == SETTING ===========================================================
    function setRoyaltyRegistry(address _royaltyRegistry) external onlyOwner {
        royaltyRegistry = IRoyalty(_royaltyRegistry);
    }

    function setReward(address _reward) external onlyOwner {
        reward = IReward(_reward);
    }

    function setMarketPayee(address _payee) external onlyOwner {
        marketPayee = _payee;
    }

    function setMarketPercent(uint16 _percent) external onlyOwner {
        require(_percent <= 10000, "percent must be less than 10000");
        marketPercent = _percent;
    }

    function setFeeRegistry(address _registry) external onlyOwner {
        feeRegistry = IMarketFee(_registry);
    }

    // == FEATURES ==========================================================
    function buyNFT(
        IMarketplace.Listing calldata _listing,
        bytes calldata _signature
    ) external payable nonReentrant {
        address seller = ECDSAUpgradeable.recover(
            _hashListing(_listing),
            _signature
        );

        require(seller == _listing.seller, "Invalid signature");
        require(
            IERC721(_listing.tokenAddress).ownerOf(_listing.tokenId) == seller,
            "Seller not own nft"
        );
        require(msg.value == _listing.price, "Price not match");

        IERC721(_listing.tokenAddress).transferFrom(
            seller,
            msg.sender,
            _listing.tokenId
        );

        uint256 balance = msg.value;
        balance = balance.sub(_payMarketFee(msg.value, _listing.tokenAddress));
        balance = balance.sub(_payRoyalty(msg.value, _listing.tokenAddress));
        _paySeller(balance, seller);

        _tryReward(seller, msg.sender, _listing.tokenAddress, _listing.price);

        emit NFTBought(
            msg.sender,
            seller,
            _listing.tokenAddress,
            _listing.tokenId,
            _listing.price
        );
    }

    function buyBundle(
        IMarketplace.Listing[] calldata _listings,
        bytes[] calldata _signatures
    ) external payable nonReentrant {
        require(_listings.length == _signatures.length, "Invalid length");
        require(_listings.length > 0, "Empty listings");
        require(_listings.length <= 20, "20 listings max");

        uint256 total = 0;
        for (uint256 i = 0; i < _listings.length; i++) {
            address seller = ECDSAUpgradeable.recover(
                _hashListing(_listings[i]),
                _signatures[i]
            );
            require(seller == _listings[i].seller, "Invalid signature");
            require(
                IERC721(_listings[i].tokenAddress).ownerOf(
                    _listings[i].tokenId
                ) == seller,
                "Seller not own nft"
            );
            total = total.add(_listings[i].price);
        }

        require(msg.value == total, "Price not match");

        for (uint256 i = 0; i < _listings.length; i++) {
            IERC721(_listings[i].tokenAddress).transferFrom(
                _listings[i].seller,
                msg.sender,
                _listings[i].tokenId
            );

            uint256 balance = _listings[i].price;
            balance = balance.sub(
                _payMarketFee(balance, _listings[i].tokenAddress)
            );
            balance = balance.sub(
                _payRoyalty(_listings[i].price, _listings[i].tokenAddress)
            );
            _paySeller(balance, _listings[i].seller);

            _tryReward(
                _listings[i].seller,
                msg.sender,
                _listings[i].tokenAddress,
                _listings[i].price
            );

            emit NFTBought(
                msg.sender,
                _listings[i].seller,
                _listings[i].tokenAddress,
                _listings[i].tokenId,
                _listings[i].price
            );
        }
    }

    function buyPackage(
        address _tokenAddress,
        uint256[] calldata _tokenIds,
        address _seller,
        uint256 _id,
        bytes calldata _signature
    ) external payable nonReentrant {
        require(_tokenIds.length > 0, "Empty listings");

        address seller = ECDSAUpgradeable.recover(
            _hashPackage(_tokenAddress, _tokenIds, _seller, msg.value, _id),
            _signature
        );
        require(seller == _seller, "Invalid signature");

        for (uint256 i = 0; i < _tokenIds.length; i++) {
            IERC721(_tokenAddress).transferFrom(
                _seller,
                msg.sender,
                _tokenIds[i]
            );
        }

        uint256 balance = msg.value;
        balance = balance.sub(_payMarketFee(balance, _tokenAddress));
        balance = balance.sub(_payRoyalty(msg.value, _tokenAddress));
        _paySeller(balance, _seller);

        _tryReward(_seller, msg.sender, _tokenAddress, msg.value);

        emit PackageBought(msg.sender, _seller, msg.value, _id);
    }

    // == HELPERS ===========================================================
    function _payMarketFee(uint256 price, address tokenAddress)
        internal
        returns (uint256)
    {
        uint256 marketRevenue = price.mul(marketPercent).div(10000);

        if (address(feeRegistry) != address(0)) {
            uint256 feePercent = feeRegistry.getFee(tokenAddress);
            if (feePercent > 0) {
                marketRevenue = price.mul(feePercent).div(10000);
            } else {
              marketRevenue = price.mul(marketPercent).div(10000);
            }
        }

        payable(marketPayee).transfer(marketRevenue);
        return marketRevenue;
    }

    function _payRoyalty(uint256 price, address tokenAddress)
        internal
        returns (uint256)
    {
        if (address(royaltyRegistry) == address(0)) {
            return 0;
        }

        address collectionPayee = royaltyRegistry.getCollectionPayee(
            tokenAddress
        );
        uint16 collectionRoyalty = royaltyRegistry.getCollectionRoyalty(
            tokenAddress
        );
        if (collectionPayee != address(0) && collectionRoyalty > 0) {
            uint256 collectionRevenue = price.mul(collectionRoyalty).div(10000);
            payable(collectionPayee).transfer(collectionRevenue);

            return collectionRevenue;
        }

        return 0;
    }

    function _paySeller(uint256 balance, address seller) internal {
        payable(seller).transfer(balance);
    }

    function _tryReward(
        address _seller,
        address _buyer,
        address _tokenAddress,
        uint256 _price
    ) internal {
        if (address(reward) == address(0)) {
            return;
        }
        try reward.reward(_seller, _buyer, _tokenAddress, _price) {} catch {}
    }

    function _hashListing(IMarketplace.Listing calldata _listing)
        private
        view
        returns (bytes32)
    {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "ListParams(address tokenAddress,uint256 tokenId,uint256 price,address seller)"
                        ),
                        _listing.tokenAddress,
                        _listing.tokenId,
                        _listing.price,
                        _listing.seller
                    )
                )
            );
    }

    function _hashPackage(
        address _tokenAddress,
        uint256[] calldata _tokenIds,
        address _seller,
        uint256 _price,
        uint256 _id
    ) private view returns (bytes32) {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "PackageParams(address tokenAddress,uint256[] tokenIds,address seller,uint256 price,uint256 id)"
                        ),
                        _tokenAddress,
                        keccak256(abi.encodePacked(_tokenIds)),
                        _seller,
                        _price,
                        _id
                    )
                )
            );
    }
}
