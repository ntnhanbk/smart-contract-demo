// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

// smart contract interface for the Offer NFT
// need Escrow contract to hold the funds
interface IOffer {

  struct Offer {
    address tokenAddress;
    uint256 tokenId;
    uint256 price;
    address bidder;
  }

  struct CounterOffer {
    address tokenAddress;
    uint256 tokenId;
    uint256 price;
    address seller;
  }

  struct PackageOffer {
    address tokenAddress;
    uint256[] tokenIds;
    uint256 price;
    address bidder;
    uint256 id;
  }

  function acceptOffer(
    IOffer.Offer calldata _offer,
    bytes calldata _signature
  ) external;

  function acceptCollectionOffer(
    IOffer.Offer calldata _offer,
    bytes calldata _signature
  ) external;

  function acceptCounterOffer(
    IOffer.CounterOffer calldata _counterOffer,
    bytes calldata _signature
  ) external;

  function acceptPackageOffer(
    IOffer.PackageOffer calldata _packageOffer,
    bytes calldata _signature
  ) external;

  // == EVENTS ===========================================================
  event OfferAccepted(
    address indexed _tokenAddress,
    uint256 _tokenId,
    uint256 _price,
    address indexed _bidder,
    address indexed _seller
  );

  event CounterOfferAccepted(
    address indexed _tokenAddress,
    uint256 _tokenId,
    uint256 _price,
    address indexed _bidder,
    address indexed _seller
  );

  event PackageOfferAccepted(
    address indexed _tokenAddress,
    uint256 _price,
    address indexed _bidder,
    address indexed _seller,
    uint256 _id
  );
}
