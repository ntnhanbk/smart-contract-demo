// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "./IMarketFee.sol";

contract MarketFee is IMarketFee, Initializable, OwnableUpgradeable {
    function initialize() public initializer {
        __Ownable_init();
    }

    mapping(address => uint16) public collectionFee;

    function getFee(address _collection)
        external
        view
        override
        returns (uint16)
    {
        return collectionFee[_collection];
    }

    function setFee(address _collection, uint16 _fee)
        external
        override
        onlyOwner
    {
        require(_fee <= 10000, "Fee must be less than 10000");
        collectionFee[_collection] = _fee;
        emit FeeRegistered(_collection, _fee);
    }
}
