// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

interface IMarketFee {
    function getFee(address) external view returns (uint16);

    function setFee(address _collection, uint16 _fee) external;

    event FeeRegistered(address indexed _collection, uint16 _fee);
}
