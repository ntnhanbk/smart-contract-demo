// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/EIP712Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";

contract Quest is
    Initializable,
    ReentrancyGuardUpgradeable,
    OwnableUpgradeable,
    EIP712Upgradeable
{
    // questId => user => claimed
    mapping(uint256 => mapping(address => bool)) public claimed;

    address public signerAddress;

    mapping(uint256 => bool) public claimedIds;

    event Claimed(
        uint256 indexed questId, // claimId
        address indexed rewardCoin,
        uint256 rewardAmount,
        address indexed user
    );

    function initialize(address _signer) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();
        __EIP712_init("Quest", "1.0.0");
        signerAddress = _signer;
    }

    function setSigner(address _signer) external onlyOwner {
        signerAddress = _signer;
    }

    function claim(
        uint256 claimId,
        address rewardCoin,
        uint256 rewardAmount,
        bytes calldata _signature
    ) external nonReentrant {
        require(!claimedIds[claimId], "Quest: already claimed");
        require(
            SignatureChecker.isValidSignatureNow(
                signerAddress,
                _hash(claimId, rewardCoin, rewardAmount, msg.sender),
                _signature
            ),
            "Quest: invalid signature"
        );

        claimedIds[claimId] = true;

        IERC20(rewardCoin).transfer(msg.sender, rewardAmount);
        emit Claimed(claimId, rewardCoin, rewardAmount, msg.sender);
    }

    function withdraw() external onlyOwner {
        uint256 balance = address(this).balance;
        payable(msg.sender).transfer(balance);
    }

    function withdrawERC20(address tokenAddress) external onlyOwner {
        uint256 balance = IERC20(tokenAddress).balanceOf(address(this));
        IERC20(tokenAddress).transfer(msg.sender, balance);
    }

    function _hash(
        uint256 _questId,
        address _rewardCoin,
        uint256 _rewardAmount,
        address _user
    ) private view returns (bytes32) {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256(
                            "Claim(uint256 questId,address rewardCoin,uint256 rewardAmount,address user)"
                        ),
                        _questId,
                        _rewardCoin,
                        _rewardAmount,
                        _user
                    )
                )
            );
    }
}
